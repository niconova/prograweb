//topnav se hace responsivo hacia abajo en pantallas pequeñas
function topResponsive() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

//esconder texto y numero en pantallas pequeñas
//centrar iconos de redes sociales
document.addEventListener("DOMContentLoaded", () => {
    let numero = document.querySelector('.numero');
    let texto = document.querySelector('.texto');
    let icons = document.querySelector('.icons');
    
    window.onresize = () => {
        if (window.innerWidth <= 768) {
            numero.style.display = "none";
            texto.style.display = "none";
            icons.classList.add("iconSmall");
        } else {
            numero.style.display = "block";
            texto.style.display = "block";  
        }
    }
})

//validar correo
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
